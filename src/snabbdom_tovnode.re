[@bs.module "snabbdom/es/tovnode"]
external vnode_of_el:
  (Dom.node, ~dom_api: Snabbdom_dom_api.t=?, unit) => Snabbdom_h.vnode =
  "toVNode";
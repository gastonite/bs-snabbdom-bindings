type t = Js.Dict.t(bool);

[@bs.module "snabbdom/modules/class"]
external snabbdomModule: Snabbdom_module.t = "ClassModule";

let make =
    (~active: array(string)=[||], ~inactive: array(string)=[||], ()): t => {
  let classNames = Js_dict.empty();
  Js_array.forEach(
    className => Js_dict.set(classNames, className, true),
    active,
  );
  Js_array.forEach(
    className => Js_dict.set(classNames, className, false),
    inactive,
  );
  classNames;
};
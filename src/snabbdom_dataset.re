[@bs.module "snabbdom/modules/clasdatasets"]
external snabbdomModule: Snabbdom_module.t = "DatasetModule";

type t = Js.Dict.t(string);

[@bs.get] external eventTargetDatasetGet: Dom.eventTarget => t = "dataset";